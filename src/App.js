import React, {useState} from 'react';
import ReactDOM from 'react-dom';
import './App.css';
import DatePicker from 'react-date-picker';
import PhoneInput from "react-phone-number-input/input";

function App() {
	const [name , setName] = useState('');
	const [collegeName , setCollegeName] = useState('');
	const [branchName, setBranchName] = useState('');
	const [email , setEmail] = useState('');
	const [value, onChange] = useState(new Date());
	const [phoneValue, setValue] = useState()
	const [gender, setGender] = useState('');
	const [address, setAddress] = useState('');

	
	const handleChange =(e)=>{
	setName(e.target.value);
	}
	
	const handleCollegeChange =(e)=>{
	setCollegeName(e.target.value);
	}
	
	const handleBranchChange =(e)=>{
	setBranchName(e.target.value);
	}
	const handleGenderChange =(e)=>{
		setGender(e.target.value);
	}
	const handleEmailChange =(e)=>{
	setEmail(e.target.value);
	}

	const handleAddressChange =(e)=>{
		setAddress(e.target.value);
	}
	
	
	
	const handleSubmit=(e)=>{
        e.preventDefault();
        
        const root = ReactDOM.createRoot(document.getElementById('root')); 
        root.render(<div>Form Submitted</div>);
	}
return (
	<div className="App">
	<header className="App-header">
	<form onSubmit={(e) => {handleSubmit(e)}}>
	
	
	<h3> Admission Form </h3>
	
		<label >
		Name:
		</label><br/>
		<input type="text" value={name} required onChange={(e) => {handleChange(e)}} /><br/>
		
		<label >
		College Name:
		</label><br/>
		<input type="text" value={collegeName} required onChange={(e) => {handleCollegeChange(e)}} /><br/>
			
		<label>
		Branch Name:
		</label><br/>
		<select value={branchName} onChange={(e) => {handleBranchChange(e)}}>
			<option value="Select your branch">Select your branch </option>
			<option value="mechanical">Mechanical</option>
			<option value="civil">Civil</option>
			<option value="cse">CSE</option>
			<option value="ece">ECE</option>
		</select><br/>
        <div value={gender} onChange={(e) => {handleGenderChange(e)}}>
		<label>Gender:
			<label><input type="radio" value="male" name="gender"/>
				Male
			</label>
			<label><input type="radio" value="female" name="gender"/>
				Female
			</label>
		</label>
		</div>
		
		<label>DOB:</label>
		<div className='Dob'>
          <DatePicker onChange={onChange} value={value} />
        </div>
		
		<label>
		Email:
		</label><br/>
		<input type="email" value={email} required onChange={(e) => {handleEmailChange(e)}} /><br/>
		
		<label>
		Address:<br/>
		<textarea value={address} onChange={(e) => {handleAddressChange(e)}} rows={5}></textarea>
		</label><br/>
		
		<label>Phone Number:
        </label><br/>
			<PhoneInput country="US" value={phoneValue} onChange={setValue} /><br/>
        
		<input type="submit" value="Submit"/>
	</form>
	</header>
	</div>
);
}

export default App;

